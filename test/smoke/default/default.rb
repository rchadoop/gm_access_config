# # encoding: utf-8

# Inspec test for recipe gm_hadoop_gdss_prodm::default


control 'Access_conf-1.3' do
  impact 0.3
  title 'Verify Root Access in access.conf'
  desc 'Checking Root Allowed access from Management Server in access.conf'
  describe file("/etc/security/access.conf") do
    its("content") { should_not match /\+ \: ALL \: ALL/ }
  end
end


