default['sys']['pam']['access'] = [

 '+ : root : LOCAL',
 '+ : ALL : cron crond',
 '+ : (localusers) : ALL' ,
 '+ : @' + node['hostname'] + ': ALL'

]

default['sys']['custom']['access_default'] 
default['sys']['custom']['access'] 
default['sys']['pam']['access_default'] = 'deny'
