#
# Cookbook Name:: sys
# Recipe:: pam
#
# Copyright 2013, Bastian Neuburger,  Victor Penso
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#
# access rules
#
#puts "in coobkbook = "
#puts  run_context.cookbook_collection[cookbook_name]
#puts  " here should be gm_access_config  #{cookbook_name} "

template '/etc/security/access.conf' do
  source 'etc_security_access.conf.erb'
  owner 'root'
  group 'root'
  mode "0600"
  variables(
    rules:   node['sys']['pam']['access'],
    custom:  node['sys']['custom']['access'],
    custom_default:  node['sys']['custom']['access_default'],
    default: node['sys']['pam']['access_default']
  )
  only_if do
    node['sys']['pam']['access'] ||
      node['sys']['pam']['access_default'] == 'deny'
  end
end

