# gm_access_config Cookbook

-  This cookbook configures the /etc/security/access.conf, with the values in the gm_access_config/attributes directory, 
-   This is the default file to build the "standard" GM /etc/security/access.conf file.
-   The purpose of this cookbook is to produce the base /etc/security/access.conf file, and additional wrapper cookbooks 
    can extend the base settings.

## Requirements

- Valid chef environment

### Platforms

- OEL 6.9 (Probably is not specific that just that, but only tested here)

### Chef

- Chef 12.0 or later

### Cookbooks

- No dependencies on other cookbooks

## Attributes
- At present, the base attributes are given below ...

 \+ : root : LOCAL

 \+ : ALL : cron crond

 \+ : (localusers) : ALL

 \+ : @' + node['hostname'] + ': ALL

 \- : ALL : ALL
   

## Usage

This cookbook is comprised of 1 recipe:   

   *  default.rb    
      This recipe defines the template, and sets the appropriate variables.
   
   
     
## License and Authors

Authors: Rick Connelly & James Mullaney

